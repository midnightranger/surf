# surf
My personal build of suckless.org surf web browser based on WebKit2/GTK+, slightly patched.

Patches been applied include:

1) bookmarking: Adds bookmarking functionality. Uses cat to add all strings from ~/.surf/bookmarks to the input dmenu. Ctrl-m is used to add a new bookmark.
